# gitlab-ci-lint

Wrapper for `.gitlab-ci.yml` linting using *gitlab.com* written in pure bash with *curl* and *jq*.

## Installation

```bash
curl \
	-SfL https://gitlab.com/blurayne/gitlab-ci-lint/-/raw/master/gitlab-ci-lint \
	-o - | install -m 755 /dev/stdin $HOME/.local/bin/gitlab-ci-lint
```

## Usage

Lint `.gitlab-ci-yml` in current directory

```bash
gitlab-ci-lint
```

Using  `.gitlab-ci-yml` as argument

```bash
gitlab-ci-lint .gitlab-ci.yml
```

Or by piping into

```bash
cat .gitlab-ci.yml | gitlab-ci-lint
```

